async function getBooks() {
    let url = 'users.json';
    try {
        let res = await fetch("https://yops0k522h.execute-api.us-east-2.amazonaws.com/books");
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}

async function renderBooks() {
    let books = (await getBooks()).Items;
    console.log(books);
    let html = '';
    let n = books.length;
    for (var i = 0; i < n; i++){
        const book = books[i];
        let htmlSegment = `<div>
                            <h1>${book.id}</h1>
                            <div>${book.title} written by ${book.author}</div>
                            <div>Category : ${book.category} </div>
                        </div>`;

        html += htmlSegment;
    }
    let container = document.querySelector('.books');
    container.innerHTML += html;
}

renderBooks();